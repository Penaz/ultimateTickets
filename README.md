# ultimateTickets
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Build Status](https://travis-ci.org/Penaz91/ultimateTickets.svg?branch=master)](https://travis-ci.org/Penaz91/ultimateTickets) ![Status:Active](https://img.shields.io/badge/Project_Status-Active-brightgreen.svg)

Yet another SQLite Ticket manager for Minecraft 1.11.2/1.12, supports Ticket Teleporting, multiple teleport hotspots per ticket, customizable Labels, search. UUID-based.
